This application prints the some of various life cycle methods of `NSApplicationDelegate`,
`NSWindowController`, and `NSWindowController`.

```swift
// Application initialization
AppDelegate init()
AppDelegate awakeFromNib()

// Object initialization
ChildVC init(coder:)
WindowController init(coder:)
ParentVC init(coder:)

// Window and View loading
WindowController windowWillLoad()
ChildVC awakeFromNib()
ChildVC viewDidLoad()
ParentVC viewDidLoad()
WindowController windowDidLoad()

// Awake from nib
WindowController awakeFromNib()
ChildVC awakeFromNib()
ParentVC awakeFromNib()

ParentVC viewWillAppear()
ChildVC viewWillAppear()
// *Views appear*
ParentVC viewDidAppear()
ChildVC viewDidAppear()

AppDelegate applicationDidFinishLaunching(_:)

ParentVC viewWillDisappear()
ChildVC viewWillDisappear()
// *Views disappear*
ParentVC viewDidDisappear()
ChildVC viewDidDisappear()
```
