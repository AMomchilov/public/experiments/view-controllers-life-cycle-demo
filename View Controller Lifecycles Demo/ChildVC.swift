//
//  ChildVC.swift
//  View Controller Lifecycles Demo
//
//  Created by Alexander Momchilov on 2020-08-03.
//  Copyright © 2020 Alexander Momchilov. All rights reserved.
//

import AppKit

class ChildVC: NSViewController {
	required init?(coder: NSCoder) {
		print(type(of: self), #function)
		super.init(coder: coder)
	}
	
	override func awakeFromNib() {
		super.awakeFromNib()
		print(type(of: self), #function)
	}
	
	override func viewDidLoad() {
		super.viewDidLoad()
		print(type(of: self), #function)
	}
	
	override func viewWillAppear() {
		super.viewWillAppear()
		print(type(of: self), #function)
		print("// *Views appear*")
	}
	
	override func viewDidAppear() {
		super.viewDidAppear()
		print(type(of: self), #function)
	}
	
	override func viewWillDisappear() {
		super.viewWillDisappear()
		print(type(of: self), #function)
		print("// *Views disappear*")
	}
	
	override func viewDidDisappear() {
		super.viewDidDisappear()
		print(type(of: self), #function)
	}
	
	override func shouldPerformSegue(
		withIdentifier identifier: NSStoryboardSegue.Identifier,
		sender: Any?
	) -> Bool {
		print(type(of: self), #function)
		return true
	}
	
	override func performSegue(
		withIdentifier identifier: NSStoryboardSegue.Identifier,
		sender: Any?
	) {
		print(type(of: self), #function)
	}
	
	override func prepare(for segue: NSStoryboardSegue, sender: Any?) {
		print(type(of: self), #function)
		print(segue.sourceController)
		print(segue.destinationController)
	}
	
	deinit {
		print(type(of: self), #function)
	}
}
