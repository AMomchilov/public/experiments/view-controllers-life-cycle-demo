//
//  AppDelegate.swift
//  View Controller Lifecycles Demo
//
//  Created by Alexander Momchilov on 2020-08-03.
//  Copyright © 2020 Alexander Momchilov. All rights reserved.
//

import Cocoa

@NSApplicationMain
class AppDelegate: NSObject, NSApplicationDelegate {

	override init() {
		super.init()
		print("// Application initialization")
		print(type(of: self), #function)
	}
	
	override func awakeFromNib() {
		super.awakeFromNib()
		print(type(of: self), #function)
		print()
		print("// Object initialization")
	}

	func applicationDidFinishLaunching(_ aNotification: Notification) {
		print()
		print(type(of: self), #function)
		print()
	}
	
	func applicationShouldTerminateAfterLastWindowClosed(_ sender: NSApplication) -> Bool {
		true
	}
}
