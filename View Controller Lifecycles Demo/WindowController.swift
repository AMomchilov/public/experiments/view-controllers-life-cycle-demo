//
//  WindowController.swift
//  View Controller Lifecycles Demo
//
//  Created by Alexander Momchilov on 2020-08-03.
//  Copyright © 2020 Alexander Momchilov. All rights reserved.
//

import AppKit

class WindowController: NSWindowController {
	required init?(coder: NSCoder) {
		print(type(of: self), #function)
		super.init(coder: coder)
	}
	
	override func awakeFromNib() {
		super.awakeFromNib()
		print(type(of: self), #function)
	}
	
	override func windowWillLoad() {
		super.windowWillLoad()
		print()
		print("// Window and View loading")
		print(type(of: self), #function)
	}
	
	override func windowDidLoad() {
		super.windowDidLoad()
		print(type(of: self), #function)
		print()
		print("// Awake from nib")

		DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
			self.window!.close()
		}
	}
	
	override func shouldPerformSegue(
		withIdentifier identifier: NSStoryboardSegue.Identifier,
		sender: Any?
	) -> Bool {
		print(type(of: self), #function)
		return true
	}
	
	override func performSegue(
		withIdentifier identifier: NSStoryboardSegue.Identifier,
		sender: Any?
	) {
		print(type(of: self), #function)
	}
	
	override func prepare(for segue: NSStoryboardSegue, sender: Any?) {
		print(type(of: self), #function)
		print(segue.sourceController)
		print(segue.destinationController)
	}
	
	deinit {
		print(type(of: self), #function)
	}
}
